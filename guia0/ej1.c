#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

bool isequal(float x, float y){
	return x == y;
}

int main(int argc, char ** argv){

	if (argc != 3+1){
		printf("Por favor introduzca los 3 lados del triangulo.\n");
		return 1;
	}

	float a = atof(argv[1]);
	float b = atof(argv[2]);
	float c = atof(argv[3]);

	bool a_igual_b = isequal(a,b);
	bool a_igual_c = isequal(a,c);
	bool b_igual_c = isequal(b,c);

	if (a_igual_b && b_igual_c){
		printf("El triangulo es equilatero.\n");			
	}
	else {
		if (a_igual_b || b_igual_c){
			printf("El triangulo es isoceles.\n");
		}
		else{
			printf("El tiangulo es escaleno.\n");
		}
	}



	return 0;
}
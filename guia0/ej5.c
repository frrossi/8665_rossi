#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>


#define n 10

int main(int argc, char ** argv){

	if (argc != 2){
		printf("Please enter only 1 argument, with an int number to look-up to.\n");
		return 1;
	}

	int number = atoi(argv[1]);

	//int n = 10; // largo del array
	// int vector[n];
	int vector[n] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	// for (i=1; i == n; i++){
	// 	vector
	// }

	for (int i=0; i==n; i++){
		if(vector[i] == number){
			printf("El numero %d tiene indice %d en la lista.\n", number, i);
		}
	}

	return 0;
}
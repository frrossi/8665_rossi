# Este directorio encapsula todos los servicios necesarios para correr el smart_home

Las configuraciones de las ejecuciones estan definidas en el archivo [docker-compose.yml](docker-compose.yml). Tanto que puertos disponibiliza y los volumenes necesarios a bindear.

Para ejecutar los servicios correr:

```{bash}
docker compose up -d
```

Para ingresar a algún contenedor:

```{bash}
docker exec -it <container_name> sh
```

## mosquitto

> Imagen base: https://hub.docker.com/_/eclipse-mosquitto

La imagen de mosquitto corre con la configuración definida en [mosquitto.conf](mosquitto/config/mosquitto.conf).

Entre otras cosas se puede definir el [método de autenticación con el broker](https://mosquitto.org/documentation/authentication-methods/).

Para crear una contraseña correr:

```{bash}
mosquitto_passwd -c <password file> <username>
```
Luego, quedará guardada en [pwfile](mosquitto/config/pwfile) encriptada.

### Testing

Para testear que el broker este funcionando correctamente podemos suscribir a un tópico:

```{bash}
mosquitto_sub -h localhost -p 1883 -t /home/pepe -u <usr> -P <pw>
```

Y luego, publicar algo a ese tópico desde otra terminal u cualquier lugar que sea posible publicar al broker, e.g. una aplicación mobile.

```{bash}
mosquitto_pub -h localhost -p 1883 -t /home/pepe -u <user> -P <password> -m "hola"
```

en caso de querer hacerlo desde fuera del contenedor `localhost` deberá ser reemplazado por la ip del host en la red local.

## Node.js (backend server)

> Imagen base: https://hub.docker.com/_/eclipse-mosquitto

La ejecución puede ser para dev o para prod, comentar o descomentar el docker-compose...


## PostgreSQL
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const PORT = 3000
const HOST = '0.0.0.0'

const app = express()

app.set('trust proxy', true)

// body parser, JSON data
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// CORS

app.use(cors({
    credentials: true,
    origin: true
}))

const apiv1 = require('./apiv1')
app.use(apiv1)

app.listen(PORT, HOST, () => {
    console.log(`Running on http://${HOST}:${PORT}`)
})
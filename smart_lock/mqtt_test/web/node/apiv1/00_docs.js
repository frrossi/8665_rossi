
const swaggerOptions = {
    swaggerDefinition: {
        swagger: '2.0',
        info: {
            version: '1.0.0',
            title: 'Smart Home API',
            description: 'API for CRUD to postgres db',
            contact:{
                name: 'Francisco Rossi',
                url: 'frrossi@fi.uba.ar'
            }
        }
    },

    apis: ['./apiv1/*.js']
}

module.exports = swaggerOptions


/**
 * @swagger
 * tags:
 *   - name: Database
 *     description: Objeto Base de Datos
 *   - name: Users
 *     description: Objeto Usuario
 */

/**
 * @swagger
 * definitions:
 *    table:
 *      name: table
 *      in: path
 *      description: Specify if
 *      required: true
 *      example: mqtt, pepe
 *    userInfo:
 *      name: body
 *      in: body
 *      description: user object to be added or updated
 *      required: true
 *      schema:
 *        type: object
 *        properties:
 *          name:
 *            type: string
 *            example: Francisco Rossi
 *          email:
 *            type: string
 *            example: frrossi@fi.uba.ar
 *          serial_number:
 *            type: string
 *            example: 0x3b7038c6b5
 */

/**
 * @swagger
 * securityDefinitions:
 *   apiKey:
 *     type: apiKey
 *     name: X-API-KEY
 *     in: header
 */
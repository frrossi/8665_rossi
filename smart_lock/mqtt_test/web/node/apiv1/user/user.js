const { Pool, Client } = require("pg");

const read_json = require('../functions/read_json')

class User {
    cfg
    req
    response

    constructor(req, response){
        this.req = req
        this.response = response
        this.pool = new Pool(read_json('/app/apiv1/config/db.json'))

        //this.response.username = eq.params.user ? req.params.user.toLowerCase() : null
        this.response.data.user = {}
        this.response.data.user.email = req.params.user ? req.params.user : null

        this.response.metadata.user_info= req.body ? req.body : null
        this.user_info = this.response.metadata.user_info

        this.response.data.table = 'users'
        this.response.data.out = {}
}
    getUsers(callback){
        this.response.setStack("getUsers")
        
        this.pool.connect((err, client, done) => {
            if (err){
            console.log(err)
            this.response.setErr(err)
            this.response.setCode(500)
            callback(this.response)
            }
            else{
            const query = `SELECT * FROM smart_home.${this.response.data.table}`
            client.query(query, [], (err, res) => {
                done()
                //console.log(res.rows)
                if (err) {
                this.response.setErr(err.stack)
                this.response.setCode(500)
                
                } else {
                this.response.data['out']['users'] = res.rows
                this.response.setCode(200)
                this.response.setMsg("Usuarios OK")
                }
                //console.log(this.response)
                callback(this.response)
            })
            }
        })
        
    }

    getUser(callback){
        this.response.setStack("getUser")
        
        this.pool.connect((err, client, done) => {
            if (err){
            console.log(err)
            this.response.setErr(err)
            this.response.setCode(500)
            callback(this.response)
            }
            else{
            const email = this.response.data.user.email
            const query = `SELECT * FROM smart_home.${this.response.data.table} WHERE email = $1`
            client.query(query, [email], (err, res) => {
                done()
                //console.log(res.rows)
                if (err) {
                this.response.setErr(err.stack)
                this.response.setCode(500)
                
                } else {
                const users = res.rows 
                if(users.length == 1){
                    this.response.data['out']['user'] = res.rows
                    this.response.setCode(200)
                    this.response.setMsg("Usuarios OK")
                }
                else if (users.length == 0){
                    this.response.setCode(404)
                    this.response.setMsg(`User with e-mail:${email} not found`)
                }
                else{
                    this.response.setCode(500)
                    this.response.setErr(`User with e-mail:${email} repeated more than once`)

                }

                }
                //console.log(this.response)
                callback(this.response)
            })
            }
        })
        
    }
    // console.log(this.response)
    // callback(this.response)

    createUser(callback){
        this.response.setStack("createUser")
        
        this.pool.connect((err, client, done) => {
            if (err){
                console.log(err)
                this.response.setErr(err)
                this.response.setCode(500)
                callback(this.response)
            }
            else{
                // const username = this.response.data.user.username
                // const email = this.response.data.user.email
                // const serial_number = this.response.data.user.serial_number
                if (this.user_info){
                    const username = this.user_info.name ? this.user_info.name : null
                    const email = this.user_info.email ? this.user_info.email : null
                    const serial_number = this.user_info.serial_number ? this.user_info.serial_number : null
                    const user_data = [username, serial_number, email]
                    const query = `INSERT INTO smart_home.${this.response.data.table} (username, serial_number, email) VALUES ($1, $2, $3)`
                    client.query(query, user_data, (err, res) => {
                        if (err) {
                            this.response.setErr(err.stack)
                            this.response.setCode(500)
                            }
                        else{
                            client.query('COMMIT', err => {
                                if (err) {
                                    this.response.setErr(err.stack)
                                    this.response.setCode(500)
                                    
                                }
                                else {
                                    this.response.setCode(200)
                                    this.response.setMsg("Usuario Registrado OK")
                                }
                                callback(this.response)
                            })
                        }
                    }) 
                }
                else {
                    this.response.setCode(400)
                    this.response.setErr("ingresa los campos bien!")
                    callback(this.response)
                }
                
                

                   
            }

        })
    }


    updateUser(callback){
        this.response.setStack("updateUser")
        
        
        this.pool.connect((err, client, done) => {
            if (err){
                console.log(err)
                this.response.setErr(err)
                this.response.setCode(500)
                callback(this.response)
            }
            else{
                if (this.user_info){
                    //console.log(this.user_info)
                    //const username = this.user_info.name ? this.user_info.name : null
                    const email = this.response.data.user.email ? this.response.data.user.email : null
                    const serial_number = this.user_info.serial_number ? this.user_info.serial_number : null
                    //console.log(email)
                    //console.log(serial_number)
                    const query = `UPDATE smart_home.${this.response.data.table} SET serial_number='${serial_number}' WHERE email='${email}'`
                    
                    client.query(query, (err, res) => {
                        
                        if (err) {
                            this.response.setErr(err.stack)
                            this.response.setCode(500)
                            }
                        else{
                            //console.log("llega hasta aca3")
                            client.query('COMMIT', err => {
                                if (err) {
                                    this.response.setErr(err.stack)
                                    this.response.setCode(500)
                                    
                                }
                                else {
                                    this.response.setCode(200)
                                    this.response.setMsg(`Usuario ${email} Actualizado`)
                                }
                                
                            })
                        }
                        callback(this.response)
                    }) 
                }
                else {
                    this.response.setCode(400)
                    this.response.setErr("ingresa los campos bien!")
                    callback(this.response)
                }
                   
            }

        })
    }
}

module.exports = User
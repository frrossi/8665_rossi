const express = require('express')
const router = require('express').Router()

const auth = require('./api/auth')
const Response = require('./api/response')
const Database = require('./database/database')

const read_json = require('./functions/read_json')

/**
 * @swagger
 * /database/{table}:
 *  get:
 *    tags: [Database]
 *    summary: hace cosas
 *    description: hace cositas
 *    security:
 *      - apiKey: []
 *    parameters:
 *      - in: path
 *        name: table   # Note the name is the same as in the path
 *        required: true
 *        schema:
 *          type: string
 *        description: The table name to query
 *    responses:
 *       '200':
 *         description: Todo OK
 *       '500':
 *         description: Todo OK'NT
 */

router.get('/database/:table', (req,res,next) => { auth.check(req, res, next) }, (req, res, next) => {

    const response = new Response(req, res, next)
    const database = new Database(req, response)

    // database.connect_client(response => {
      
    //     if (response.status.code == 200){
    //         console.log("hola")
    //         database.getData(response => {
    //             response.send()
    //         })
    //     }
    //     else {
    //         response.send()
    //     }
        
    // })

    database.getData(response => {
        response.send()
    })
    
})

module.exports = router
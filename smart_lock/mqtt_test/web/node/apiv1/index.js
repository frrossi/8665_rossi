const express = require('express')
const router = require('express').Router()


const basePath = '/api/v1'

const swaggerJsDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')
const swaggerOptions = require('./00_docs.js')
swaggerOptions.swaggerDefinition.basePath = basePath
const swaggerDocs = swaggerJsDoc(swaggerOptions)
router.use(basePath, swaggerUi.serve)


// custom swagger setup
router.get(basePath, swaggerUi.setup(swaggerDocs))

router.get(basePath + '/api-docs.json', (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    res.send(swaggerDocs)
})

router.use(basePath, require('./05_base'))
router.use(basePath, require('./10_user'))

module.exports = router
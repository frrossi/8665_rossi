const read_json = require('../functions/read_json')

const auth = {
    check: function(req, res, next){
        let authorized = null
        apikeys = read_json('./apiv1/config/apikeys.json')

        if (req.headers['x-api-key'] != undefined){
            let apikey = req.headers['x-api-key']

            Object.keys(apikeys).forEach(user => {
                if (apikey == apikeys[user].apikey){
                    authorized = true
                    
                }
            })
        }
        if (authorized){
            console.log("Auth OK")
            next()
        }

        else{
            console.log("Authorization Failed")
            res.status(401).end(
            next('route')
            )
        }
    }
}

module.exports = auth
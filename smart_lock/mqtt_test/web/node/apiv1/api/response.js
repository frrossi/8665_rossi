// Clase respuesta

class Response {
    metadata
    data
    status
    #req
    #res
    #next

    constructor(req, res, next){
        this.#req = req
        this.#res = res
        this.#next = next

        this.metadata = {}
        this.data = {}
        this.status = {
            code: null,
            msg: null,
            err: null,
            stack: []
        }

    this.setStack('new')
    this.metadata.url = req.originalUrl

    }

    send(){
        this.#res.status(this.status.code || 503).json(this)
        this.#next('route')
        return
    }

    setStack(text){
        this.status.stack.push(text)
        return this
    }

    setCode(code){
        this.status.code = code
        return this
    }

    setMsg(msg){
        this.status.msg = msg
        return this
    }

    setErr(err){
        this.status.err = err
        return this
    }

}

module.exports = Response
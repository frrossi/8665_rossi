const express = require('express')
const router = require('express').Router()

const auth = require('./api/auth')

const Response = require('./api/response')
const User = require('./user/user')

const read_json = require('./functions/read_json')


/**
 * @swagger
 * /users:
 *  get:
 *    tags: [Users]
 *    summary: Get all users
 *    description: Get all users
 *    security:
 *      - apiKey: []
 *    responses:
 *       '200':
 *         description: Users displayed
 *       '500':
 *         description: Something went wrong
 */

 router.get('/users', (req,res,next) => { auth.check(req, res, next) }, (req, res, next) => {
    const response = new Response(req, res, next)
    const user = new User(req, response)
    user.getUsers(response => {
        response.send()
    })
    
})

/**
 * @swagger
 * /users/{user}:
 *  get:
 *    tags: [Users]
 *    summary: Get user
 *    description: Get one user
 *    security:
 *      - apiKey: []
 *    parameters:
 *      - in: path
 *        name: user   # Note the name is the same as in the path
 *        required: true
 *        user EMAIL:
 *          type: string
 *        description: The email of the user to get
 *    responses:
 *       '200':
 *         description: User displayed
 *       '404':
 *         description: User not found
 *       '500':
 *         description: Something went wrong
 */

 router.get('/users/:user', (req,res,next) => { auth.check(req, res, next) }, (req, res, next) => {
    const response = new Response(req, res, next)
    const user = new User(req, response)
    user.getUser(response => {
        response.send()
    })
    
})

/**
 * @swagger
 * /users/{user}:
 *  post:
 *    tags: [Users]
 *    summary: Get user
 *    description: Get one user
 *    security:
 *      - apiKey: []
 *    parameters:
 *      - in: path
 *        name: user   # Note the name is the same as in the path
 *        required: true
 *        user EMAIL:
 *          type: string
 *        description: The email of the user to get
 *      - $ref: '#/definitions/userInfo'
 *    responses:
 *       '200':
 *         description: User created/updated
 *       '401':
 *         description: No autorizado
 *       '404':
 *         description: User not found
 *       '500':
 *         description: Something went wrong
 */

 router.post('/users/:user', (req,res,next) => { auth.check(req, res, next) }, (req, res, next) => {
    const response = new Response(req, res, next)
    const user = new User(req, response)
    user.getUser(response => {
        if(response.status.code == 404){
            user.createUser(response => {
                //console.log(response.status.code)
                if(response.status.code == 200){
                    user.getUser(response => {
                        console.log(response.status.code)
                        response.send()
                    })
                }
                else{
                    response.send()
                }
            })
        }
        else{
            console.log('llama a update')
            user.updateUser(response => {
                console.log('salio d update')
                console.log(response.status.code)
                if(response.status.code == 200){
                    user.getUser(response => {
                        console.log(response.status.code)
                        response.send()
                    })
                }
                else{
                    response.send()
                }
            })
        }
    })
    
})

module.exports = router
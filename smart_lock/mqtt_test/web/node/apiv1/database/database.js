const { Pool, Client } = require("pg");

const read_json = require('../functions/read_json')

class Database {
    cfg
    req
    response


    constructor(req, response){
        this.req = req
        this.response = response
        //this.client = new Client(read_json('/app/apiv1/config/db.json'))
        // this.client = new Client({
        //   user: 'postgres',
        //   host: 'postgres',
        //   database: 'postgres',
        //   password: 'password',
        //   port: 5432
        // })
        this.pool = new Pool(read_json('/app/apiv1/config/db.json'))

        this.response.data.table = req.params.table ? req.params.table.toLowerCase() : null
        this.response.data.out = null
    }

    getData(callback){
        this.response.setStack("getData")
        
        this.pool.connect((err, client, done) => {
            if (err){
              console.log(err)
              this.response.setErr(err)
              this.response.setCode(500)
              callback(this.response)
            }
            else{
              client.query(`SELECT * FROM smart_home.${this.response.data.table}`, [], (err, res) => {
                done()
                //console.log(res.rows)
                if (err) {
                  this.response.setErr(err.stack)
                  this.response.setCode(500)
                  
                } else {
                  this.response.data['out'] = res.rows
                  this.response.setCode(200)
                  this.response.setMsg("data leida OK")
                }
                console.log(this.response)
                callback(this.response)
              })
            }
        })
        // console.log(this.response)
        // callback(this.response)
    }

    // connect_client(callback){

    //     this.response.setStack('connect')
    //     this.client.connect(function(err) {
    //         if (err) throw err;
    //         console.log("Connection to db OK")
    //     })
    //     this.response.setCode(200)
    //     callback(this.response)
        
    // }

}

module.exports = Database
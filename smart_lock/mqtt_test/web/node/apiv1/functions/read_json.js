const fs = require('fs')

function read_json(file){
    const jsonString = fs.readFileSync(file)
    return JSON.parse(jsonString)

}

module.exports = read_json
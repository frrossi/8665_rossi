package com.labi.smart_lock

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import org.json.JSONObject

class RegisterActivity : AppCompatActivity() {
    // [START declare_auth]
    private lateinit var auth: FirebaseAuth
    // [END declare_auth]


    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        // [START initialize_auth]
        // Initialize Firebase Auth
        auth = Firebase.auth
        // [END initialize_auth]


        findViewById<Button>(R.id.register_button).setOnClickListener{
            val email: String = findViewById<EditText>(R.id.register_email).text.toString()//.trim{it <= ' '}
            val password: String = findViewById<EditText>(R.id.register_password).text.toString()//.trim{it <= ' '}
            val name: String = findViewById<EditText>(R.id.register_name).text.toString()
            addUser(name, email, "")
            createAccount(email, password)

        }

        findViewById<TextView>(R.id.login_tv).setOnClickListener{
            startActivity(Intent(this@RegisterActivity, LoginActivity::class.java))
            finish()
        }

    }

    // [START on_start_check_user]
    public override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        if(currentUser != null){
            reload();
        }
    }

    // [END on_start_check_user]

    private fun createAccount(email: String, password: String) {
        // [START create_user_with_email]
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "createUserWithEmail:success")
                    val user = auth.currentUser
                    Toast.makeText(baseContext, "Account created successfully.",
                        Toast.LENGTH_SHORT).show()
                    updateUI(user, email)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "createUserWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                    updateUI(null, "")
                }
            }
        // [END create_user_with_email]
    }

    private fun signIn(email: String, password: String) {
        // [START sign_in_with_email]
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithEmail:success")
                    val user = auth.currentUser
                    updateUI(user, email)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                    updateUI(null, "")
                }
            }
        // [END sign_in_with_email]
    }

    private fun sendEmailVerification() {
        // [START send_email_verification]
        val user = auth.currentUser!!
        user.sendEmailVerification()
            .addOnCompleteListener(this) { task ->
                // Email Verification sent
            }
        // [END send_email_verification]
    }

    private fun updateUI(user: FirebaseUser?, email: String) {
        if(user != null) {
            val intent = Intent(this@RegisterActivity, MainActivity::class.java)
            intent.putExtra("email",email)
            startActivity(intent)
            finish()
        }
    }


    private fun reload() {

    }

    private fun addUser(name: String, email: String, serial_number: String){
        // ...

        // Instantiate the RequestQueue.
        val queue = Volley.newRequestQueue(this)
        val url = "http://192.168.0.121:3000/api/v1/users/${email}"
        val body = JSONObject()
        body.put("name", name)
        body.put("email", email)
        body.put("serial_number", "")
// Request a string response from the provided URL.
        val stringRequest = object: JsonObjectRequest(Request.Method.POST, url, body,
            { response ->
                // Display the first 500 characters of the response string.
                val output = "Response is: ${response}"
                Toast.makeText(baseContext, output,
                    Toast.LENGTH_SHORT).show()
            },
            { err->
                val output = "That didn't work!"
                Toast.makeText(baseContext, err.toString(),
                    Toast.LENGTH_SHORT).show()})
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["x-api-key"] = "123456789"
                headers["Content-Type"] = "application/json"
                headers["accept"] = "application/json"
                return headers
            }
        }

        // Access the RequestQueue through your singleton class.
        queue.add(stringRequest)

    }

    companion object {
        private const val TAG = "EmailPassword"
    }
}
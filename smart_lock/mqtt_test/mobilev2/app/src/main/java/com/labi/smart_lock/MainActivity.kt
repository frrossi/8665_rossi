package com.labi.smart_lock

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import protocols.UIUpdater
import com.labi.smart_lock.MQTTManager
import org.json.JSONObject

class MainActivity : Activity(), UIUpdater {

    var mqttManager:MQTTManager? = null

    // MQTT Conection
    //private val host = "tcp://" + "192.168.0.60" + ":1883"
    private val host = "tcp://" + "192.168.0.121" + ":1883"
    private val pub_topic = "/labi/main_door"
    private val register_topic = "/labi/main_door/register"
    private val topic = "/labi/response"
    private val username = "pepe"
    private val password = "jorge123"
    // Interface methods
    private fun updateConnectedLabel(status: String) {
        findViewById<TextView>(R.id.connected_label).text = status
    }
    override fun resetUIWithConnection(status: Boolean) {

        // Update the status label.
        if (status){
            updateStatusViewWith("Connected")
            updateConnectedLabel("Connected")
        }else{
            updateStatusViewWith("Disconnected")
            updateConnectedLabel("Disconnected")
        }
    }

    override fun updateStatusViewWith(status: String) {
        findViewById<TextView>(R.id.statusLabl).text = status
    }

    override fun update(message: String, topic:String) {

        if (topic == pub_topic){
            return;
        }
        var text = findViewById<EditText>(R.id.messageHistoryView).text.toString()
//        var newText = """
//            $text
//            $message
//            """
        var newText = """
            $message
            """
        //var newText = text.toString() + "\n" + message +  "\n"
        findViewById<EditText>(R.id.messageHistoryView).setText(newText)
        findViewById<EditText>(R.id.messageHistoryView).setSelection(findViewById<EditText>(R.id.messageHistoryView).text.length)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        val userEmail=intent.getStringExtra("email")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        connect();
        findViewById<Button>(R.id.open_door_button).setOnClickListener{
            openDoor()
        }

        findViewById<Button>(R.id.register_card).setOnClickListener{
            readCard(userEmail.toString())
        }

        // Enable send button and message textfield only after connection
        resetUIWithConnection(false)
    }

    private fun connect(){

            var connectionParams = MQTTConnectionParams("",host,topic,username,password)
            mqttManager = MQTTManager(connectionParams,applicationContext,this)
            mqttManager?.connect()
    }


    private fun openDoor(){
        mqttManager?.publish(pub_topic, "0")
    }

    private fun readCard(email: String){
        mqttManager?.publish(register_topic, email)
    }
}
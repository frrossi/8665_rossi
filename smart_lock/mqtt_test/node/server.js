// MQTT Connection
const mqtt = require('mqtt')
// Database Postgres Connection
const { Client } = require('pg')
const { Pool } = require('pg')
// time
const moment = require('moment')
var now = moment().utcOffset(-3)

const http = require('http')
const http_options = {
  hostname: 'node_web',
  port: 3000,
  path: '/api/v1/database/users',
  method: 'GET',
  headers: {
    'Content-Type': 'application/json',
    'x-api-key': 123456789
  }
}
var http_options_card = {
  hostname: 'node_web',
  port: 3000,
  path: '/api/v1/users/',
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    'x-api-key': 123456789
  }
}

const read_json = require('./app/functions/read_json')

// database info & connection
const schema = 'smart_home'
const table = 'mqtt_data'
const db_client_info = read_json('./config/db.json')
const db_client = new Client(db_client_info)

db_client.connect(function(err) {
  if (err) throw err;
  console.log("Connection to db OK")
})

// broker info & connection
const broker_info = read_json('./config/broker.json')
const clientId = broker_info.clientId
const connectUrl = `mqtt://${broker_info.host}:${broker_info.port}`
const client = mqtt.connect(connectUrl, {
  clientId,
  clean: true,
  connectTimeout: 4000,
  username: broker_info.username,
  password: broker_info.password,
  reconnectPeriod: 1000,
})

// subscribing to the root topic
const topic = '/#'
client.on('connect', () => {
  console.log('Connection to broker OK')
  client.subscribe([topic], () => {
    console.log(`Subscribed to topic '${topic}'`)
  })
})

// callback for message received (log to database table:schema)
client.on('message', (topic, payload) => {
    console.log('Received Message:', topic, payload.toString())

    const pool = new Pool()
    pool.connect((err, client, done) => {
      const shouldAbort = err => {
        if (err) {
          console.error('Error in transaction', err.stack)
          db_client.query('ROLLBACK', err => {
            if (err) {
              console.error('Error rolling back client', err.stack)
            }
            // release the client back to the pool
            done()
          })
        }
        return !!err
      }


      

      // loggin data.
      db_client.query('BEGIN', err => {
        if (shouldAbort(err)) return
        n = 4
        // 4 hardcodeado x la db digo 4 para poner un ejemplo seria como las divisiones de la casa

        var topic_sliced = topic.split('/').filter(e => e)
        // si tiene mas de n entonces lo boleteo
        if (topic_sliced.length > n){topic_sliced=topic_sliced.slice(0, n)}
        const item = topic_sliced.pop()
        while(topic_sliced.length < n-1){
          topic_sliced.push(null)
        }
        topic_sliced.push(item)
        var values = topic_sliced
        values.push(payload.toString())
        values.push(now.format('YYYY-MM-DD HH:mm:ss Z'))
        
        const query = `INSERT INTO ${schema}.${table} (name, level, room, item, msg, time) VALUES ($1, $2, $3, $4, $5, $6)`
        //const values = topic_sliced//[topic, payload.toString()]
        //console.log(values)
        db_client.query(query, values, (err, res) => {
          if (shouldAbort(err)) return

          db_client.query('COMMIT', err => {
            if (err) {
              console.error('Error committing transaction', err.stack)
            }
            done()
          })
        })
      })

    })

    if(topic == '/labi/users'){
      
      const req = http.request(http_options, res => {
        if(res.statusCode != 200){
          console.log(`Error Status code = ${res.statusCode}`)
          return 
        }
      
        res.on('data', d => {
          //process.stdout.write(d)
          const data = JSON.parse(d).data
          console.log(Object.keys(data))
          const users = data.out
          //users = output.data.out
          console.log(JSON.stringify(users))

          var user = users.filter(user=>{
            //console.log(user.serial_number)
            return user.serial_number == payload
           })
          
          auth = (user.length == 1);
          user = user[0]
          if(auth){
            time = now.format('YYYY-MM-DD HH:mm:ss')
            console.log(`${user.username} Authorized ${time}`)
            client.publish("/labi/main_door", `0`)
          }
          else{
            console.log(`${payload} Not Authorized`)
            client.publish("/labi/main_door", `1`)
          }
          //console.log(`SALIDA ${JSON.stringify(output.data.out)}`)
          //
        //})
      })
  
      req.on('error', error => {
        console.error(error)
      })
    })
      req.end()
    }
    else if(topic == '/labi/register/card'){
      const values = (payload.toString()).split(',')
      const serial_number = values[0]
      const email = values[1]
      postData = JSON.stringify({
        "serial_number": serial_number
      })
      http_options_card.path = `/api/v1/users/${email}`
      const req = http.request(http_options_card, res => {
        if(res.statusCode != 200){
          console.log(`Error Status code = ${res.statusCode}`)
          return 
        }
      
        res.on('data', d => {
          //process.stdout.write(d)
          const data = JSON.parse(d)
          console.log(Object.keys(data))
          //users = output.data.out
          console.log(JSON.stringify(data))
      })
  
      req.on('error', error => {
        console.error(error)
      })
    })
      req.write(postData)
      req.end()
    }

  })




  
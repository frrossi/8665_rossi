//const { Client } = require('pg')
const { Pool } = require('pg');

class Database {
    cfg
    req 

    constructor(req, response){
        this.req = req
        this.response = response
        this.pool = new Pool(read_json('/app/apiv1/config/db.json'))

        this.response.data.table = req.params.table ? req.params.table.toLowerCase() : null
    }

    getData(callback){
        this.response.setStack("getData")

        this.pool.connect((err, client, done) => {
            if (err){
              console.log(err)
              this.response.setErr(err)
              this.response.setCode(500)
            }
            else{
              client.query(`SELECT * FROM smart_home.${this.response.data.table}`, [], (err, res) => {
                done()

                if (err) {
                  this.response.setErr(err.stack)
                  this.response.setCode(500)
                } else {
                  this.response.data['Out'] = res.rows
                  this.response.setCode(200)
                  this.response.setMsg("data leida OK")
                }
              })
            }
        })
        console.log(this.response)
        callback(this.response)
    }

//   insertData(callback, table, values){
//     this.response.setStack("insertData")

//     this.pool.connect((err, client, done) => {
//         if (err){
//           console.log(err)
//           this.response.setErr(err)
//           this.response.setCode(500)
//         }
//         else{
//           client.query(`INSERT INTO smart_home.${table} (name, level, room, item, msg, time) VALUES ($1, $2, $3, $4, $5, $6)`, values, (err, res) => {
//             done()

//             if (err) {
//               this.response.setErr(err.stack)
//               this.response.setCode(500)
//             } else {
//               this.response.data['Out'] = res.rows
//               this.response.setCode(200)
//               this.response.setMsg("data leida OK")
//             }
//           })
//         }
//     })
//     console.log(this.response)
//     callback(this.response)
// }
}

module.exports = Database





// const query = `SELECT * FROM Users WHERE Id=1;`;
// QueryRow(query)
// async function QueryRow(query) {
//     try {
//       const client = await pool.connect();
//       const res = await client.query(query);
//       const rows = res.rows; 
//       messages.push(`Querying from Table and selecting user ${ JSON.stringify(rows[0]) }`);
//     } catch (err) {
//       console.error(err);
//     }
// }
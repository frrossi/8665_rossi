#include "lights.h"

static const char *TAG_light = "Lights";

void light_set(bool state, int LED){
    ESP_LOGI(TAG_light, "Lights ON/OFF\n");
    gpio_set_level(LED, state);
    return;
}
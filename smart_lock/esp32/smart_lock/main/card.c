#include "card.h"
#include "door.h"
#include "rc522.h"
#include "mqtt_client.h"
#include "mqtt.h"


static const char *TAG_card = "Card";

static bool register_flag = false;

void set_register_flag_from_card(bool value){
    register_flag = value;
}

void tag_handler(uint8_t* sn, void * callback_args) { // serial number is always 5 bytes long

    // getting MQTT client from args
    //callback_args_t* callback_args_casted = callback_args; 
    callback_args_t* callback_args_casted = (callback_args_t*)callback_args; // volatile?
    esp_mqtt_client_handle_t client = callback_args_casted->mqtt_client;

    if(!(register_flag)){
        ESP_LOGI(TAG_card, "Tag: %#x %#x %#x %#x %#x",
            sn[0], sn[1], sn[2], sn[3], sn[4]
        );
        resolve_access(sn, client);
    }
    else{
        printf("register user\n");
        light_set(true, LED_GREEN);
        char* email = get_email();
        register_user(sn, email, client);
        register_flag = false;
        vTaskDelay(1000/portTICK_RATE_MS);
        buzzer_ring(2, 100, 30);
        light_set(false, LED_GREEN);
    }

}


void resolve_access(uint8_t* sn, esp_mqtt_client_handle_t client){
    char str_sn[13]; // aca en la 51 esta rompiendo
    sprintf(str_sn, "%#x%x%x%x%x", 
        sn[0], sn[1], sn[2], sn[3], sn[4]);
    if(str_sn != NULL){
        esp_mqtt_client_publish(client, "/labi/users", str_sn, strlen(str_sn), 0, false);    
    }

}

   
void register_user(uint8_t*sn,char* email,esp_mqtt_client_handle_t client){

    char str_sn[14];
    sprintf(str_sn, "%#x%x%x%x%x", 
        sn[0], sn[1], sn[2], sn[3], sn[4]);

    char*msg = str_sn;
    msg[12] = ',';
    msg[13] = '\0';

    strcat(msg, email);

    esp_mqtt_client_publish(client, "/labi/register/card", msg, strlen(msg), 0, false);    
}
/**
 * @file storage_enums_labi.h
 * @author Gerardo Stola
 * @date 14/6/2019
 * @brief Listado de enums en NVS
 *
 */
/*--->> Definiciones <<---------------------------------------------------------*/

#ifndef __STORAGE_ENUMS_LABI_H
#define __STORAGE_ENUMS_LABI_H


typedef enum
{
  STORAGE_8_WIFI_MODO,  
  STORAGE_8_DESCARGA_TIPO,
  STORAGE_8_DESCARGA_ESTADO,  
  STORAGE_8_CANTIDAD	/* siempre al final */
} storage_8_enum;
  
typedef enum
{
  STORAGE_16_BUILD_NUMBER,  
  STORAGE_16_DESCARGA_TIMEOUT_SEGUNDOS,  
  STORAGE_16_CANTIDAD	/* siempre al final */
} storage_16_enum;

typedef enum
{      
  STORAGE_32_OTA_CRC_ARCHIVO,
  STORAGE_32_OTA_LARGO_ARCHIVO,
  STORAGE_32_CANTIDAD	/* siempre al final */
} storage_32_enum;

typedef enum
{
  STORAGE_BLOB_WIFI_PWD,
  STORAGE_BLOB_WIFI_SSID,
  STORAGE_BLOB_OTA_DOMINIO, 
  STORAGE_BLOB_CANTIDAD	/* siempre al final */
} storage_blob_enum;

#endif

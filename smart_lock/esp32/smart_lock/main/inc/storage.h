/**
 * @file storage.h
 * @author Gerardo Stola
 * @date 14/6/2019
 * @brief API de biblioteca storage
 *
 */
 
#ifdef __cplusplus
extern "C" {
#endif


#ifndef __STORAGE_H
#define __STORAGE_H

/*--->> Includes <<-----------------------------------------------------------*/
#include "sdkconfig.h"
#include "nvs_flash.h"
#include "esp_system.h"
#include "nvs.h"
#include <string.h>
#include "esp_partition.h"
#include "storage_enums_labi.h"

/*--->> Definiciones <<-------------------------------------------------------*/
/*
enum STOTQ_ERR_CODES
{
	ERR_OUT_OF_RANGE,
	ERR_NO_FLASH
};
*/
typedef enum
{
  STORAGE_OK,
  STORAGE_ERROR_FUERA_DE_RANGO,
  STORAGE_ERROR_LONGITUD_INVALIDA,
  STORAGE_ERROR_NVS
} storage_resultado;



#define STORAGE_8_BITS     "storage_8_nvs"
#define STORAGE_16_BITS    "storage_16_nvs"
#define STORAGE_32_BITS    "storage_32_nvs"
#define STORAGE_BLOB_NS    "storage_blob"
#define STORAGE_SYSTEM     "storage_sys"
#define STORAGE_BLOB_MAX_SIZE     255
#define STORAGE_TAG_MAX_SIZE 	      15
#define TRUE               1


/*--->> API <<----------------------------------------------------------------*/

/**
 * @brief Escribe en nvs una variable de 8 bits
 * 
 * Se almacena en memoria no volatil en el namespace correspondiente a 
 * a variables de 8 bits, el tag no debe ser superior a STORAGE_TAG_MAX_SIZE
 *
 *  ERR_OUT_OF_RANGE -> Si valor está fuera de rango
 *
 * @param idx 	Indice en la tabla de parámetros de 8 bits
 * @param valor  Valor a almacenar en nvs 
 *
 * @return      STORAGE_OK si no hubo error
 */
storage_resultado storage_write_8(storage_8_enum idx, uint8_t valor);



/**
 * @brief lee de nvs una variable de 8 bits
 * 
 * Se lee de nvs un dato de 8 bits , 
 *
 * @param idx 	Indice en la tabla de parámetros de 8 bits
 * @valor 	Apunta a la dirección del dato donde copia el resultado
 *
 * @return      STORAGE_OK si no hubo error
 */
storage_resultado storage_read_8(storage_8_enum idx, uint8_t* valor);


/**
 * @brief Escribe en nvs una variable de 16 bits
 * 
 * Se almacena en memoria no volatil en el namespace correspondiente a 
 * a variables de 16 bits, el tag no debe ser superior a STORAGE_TAG_MAX_SIZE
 *
 *  ERR_OUT_OF_RANGE -> Si valor está fuera de rango
 *
 * @param idx 	Indice en la tabla de parámetros de 16 bits
 * @param valor  Valor a almacenar en nvs 
 *
 * @return      STORAGE_OK si no hubo error
 */
storage_resultado storage_write_16(storage_16_enum idx, uint16_t valor);



/**
 * @brief lee de nvs una variable de 16 bits
 * 
 * Se lee de nvs un dato de 16 bits , 
 *
 * @param idx 	Indice en la tabla de parámetros de 16 bits
 * @valor  i    Apunta a la dirección del dato donde copia el resultado
 *
 * @return      STORAGE_OK si no hubo error
 */
storage_resultado storage_read_16(storage_16_enum idx, uint16_t* valor);

/**
 * @brief Escribe en nvs una variable de 32 bits
 * 
 * Se almacena en memoria no volatil en el namespace correspondiente a 
 * a variables de 32 bits, el tag no debe ser superior a STORAGE_TAG_MAX_SIZE
 *
 *  ERR_OUT_OF_RANGE -> Si valor está fuera de rango
 *
 * @param idx 	Indice en la tabla de parámetros de 32 bits
 * @param valor  Valor a almacenar en nvs 
 *
 * @return      STORAGE_OK si no hubo error
 */
storage_resultado storage_write_32(storage_32_enum idx, uint32_t valor);



/**
 * @brief lee de nvs una variable de 32 bits
 * 
 * Se lee de nvs un dato de 32 bits , 
 *
 * @param idx 	Indice en la tabla de parámetros de 32 bits
 * @valor  i    Apunta a la dirección del dato donde copia el resultado
 *
 * @return      STORAGE_OK si no hubo error
 */
storage_resultado storage_read_32(storage_32_enum idx, uint32_t* valor);

/**
 * @brief Escribe en memoria flash un string
 *        el largo máximo del blob es STORAGE_BLOB_MAX_SIZE 
 *
 * @param idx 	Indice en la tabla de parámetros de blob
 * @param * str apunta al string de caracteres a almacenar
 * @param count cantidad de elementos en el string
 *
 * @return      STORAGE_OK si no hubo error
 */
storage_resultado storage_blob_write(storage_blob_enum idx, uint8_t * str, uint16_t count);


/**
 * @brief Lee de memoria flash un string
 *        el largo máximo del blob es STORAGE_BLOB_MAX_SIZE
 *
 * @param idx 	Indice en la tabla de parámetros de blob
 * @param *str apunta al string de caracteres a almacenar
 * @param *str_largo_maximo	apunta al largo máximo del string. Si es insuficiente, devuelve
 * STORAGE_ERROR_FUERA_DE_RANGO
 *
 * @return      STORAGE_OK si no hubo error
 */

storage_resultado storage_blob_read(storage_blob_enum idx, uint8_t * str, uint16_t *str_largo_maximo);

/**
 * @brief Inicializa librería
 *
 * Configura el driver de nvs y en caso de que ser el primer 
 * boot carga los valores por defecto 
 * En caso de no poder inicializarla, intenta formatear
 * 
 * @return      STORAGE_OK si no hubo error
 */
storage_resultado storage_init(void);


/**
 * @brief       Setea los valores de fabrica de todos los bloques de memoria definidos
 * 
 * @return      STORAGE_OK si no hubo error
 */ 
storage_resultado storage_set_factory(void);

#endif

#ifdef __cplusplus
}
#endif

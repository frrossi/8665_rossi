/**
 * @file configuracion.h
 * @author Gerardo
 * @date 14/6/2019
 * @brief General configuration
 *
 */
 
/*--->> Definiciones <<---------------------------------------------------------*/

#ifndef __CONFIGURACION_H
#define __CONFIGURACION_H

#define CLOUD_DEFAULT_DOMINIO      "labi.duckdns.org"


#define	VERSION_MAYOR			0
#define	VERSION_MENOR			1
#define	VERSION_BUILD			1

#define SIMULADOR 1
#endif

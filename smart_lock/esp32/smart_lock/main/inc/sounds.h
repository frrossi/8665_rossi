#include <stdbool.h>

// ESP Core
#include "esp_system.h"
#include "esp_log.h"
// ESP FreeRTOS
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
//ESP Peripherals
#include "driver/gpio.h"
#define BUZZER_GPIO 26

void buzzer_ring(size_t reps, size_t ring_duration_ms, size_t ring_interval_ms);
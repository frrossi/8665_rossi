#ifndef _DESCARGA_H_
#define _DESCARGA_H_
#include "configuracion.h"

#define DESCARGA_MAX_DOMINIO_LEN 							50
#define DESCARGA_DEFAULT_DOMINIO 							CLOUD_DEFAULT_DOMINIO

#define DESCARGA_TIMEOUT_DEFAULT_VALOR				30
#define DESCARGA_TIMEOUT_DEFAULT_MINIMO				10
#define DESCARGA_TIMEOUT_DEFAULT_MAXIMO				3000

#define DESCARGA_NO_HAY			                  0
#define DESCARGA_OTA_CON_REINICIO      				1
#define DESCARGA_OTA_SIN_REINICIO      				2
#define DESCARGA_SIMULACION			      				3

#if SIMULADOR==1
	#define DESCARGA_ULTIMO											DESCARGA_SIMULACION
#else
	#define DESCARGA_ULTIMO											DESCARGA_OTA_SIN_REINICIO
#endif
	
#define DESCARGA_FRECUENCIA_REINTENTOS_MINUTOS 1
#define DESCARGA_FRECUENCIA_REINTENTOS_SEGUNDOS 3

typedef enum
{
	DESCARGA_ESTADO_INACTIVO,
	DESCARGA_ESTADO_DESCARGANDO,
  DESCARGA_ESTADO_VALIDANDO,
	DESCARGA_ESTADO_DESCARGADO,
} descarga_estado_e;

/**
 * @brief 
 * @param 
 * @return
 */
bool descarga_init();

/**
 * @brief 
 * @param 
 * @return
 */
bool descarga_getEstado(descarga_estado_e *estado);


/**
 * @brief 
 * @param 
 * @return
 */

bool descarga_setEstado(descarga_estado_e valor_nuevo);
/**
 * @brief 
 * @param 
 * @return
 */
void descarga_habilitaDebug(bool estado);



/**
 * @brief 
 * @param 
 * @return
 */
bool descarga_getTipoDescarga (uint8_t* tipo);

/**
 * @brief 
 * @param 
 * @return
 */
bool descarga_setTipoDescarga(uint8_t tipo);

/**
 * @brief 
 * @param 
 * @return
 */
bool descarga_SetTimeoutSegundos(uint16_t timeout_segundos);

/**
 * @brief 
 * @param 
 * @return
 */
bool descarga_leeFragmento(uint8_t **buffer, size_t *leidos, bool * flag_cerrarArchivo);

/**
 * @brief 
 * @param 
 * @return
 */
bool descarga_leeLinea(uint8_t **buffer, size_t *indice, bool *flag_archivoCerrado, uint16_t * nro_linea);

/**
 * @brief 
 */
void descarga_descartar();

#endif


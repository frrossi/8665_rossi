/**
 * @file storage_parametros.h
 * @author Gerardo Stola
 * @date 14/6/2019
 * @brief Listado de parámetros en NVS
 *
 */
 
/*--->> Definiciones <<---------------------------------------------------------*/

#ifndef __STORAGE_PARAMETROS_H
#define __STORAGE_PARAMETROS_H
#include "descarga.h"

#include "configuracion.h"

#include "red.h"
  
STORAGE_8_INICIO
//STORAGE_8(variable,                                etiqueta,       default,                     minimo,                      maximo,                    opciones )
  STORAGE_8(STORAGE_8_WIFI_MODO,                     "wifi-modo",    RED_WIFI_MODO_AP,            RED_WIFI_MODO_AP,            RED_WIFI_MODO_ESTACION,      1)
  STORAGE_8(STORAGE_8_DESCARGA_TIPO,            		 "desc-tipo",    DESCARGA_NO_HAY, 						DESCARGA_NO_HAY, 						 DESCARGA_ULTIMO, 					0)
	STORAGE_8(STORAGE_8_DESCARGA_ESTADO,               "ota-estado",   DESCARGA_ESTADO_INACTIVO,    DESCARGA_ESTADO_INACTIVO,    DESCARGA_ESTADO_DESCARGADO,                 0)  
STORAGE_8_FIN


STORAGE_16_INICIO
  // STORAGE_16( variable,                      			etiqueta,      	valor,                       			minimo,                     			maximo,                               opciones)
  STORAGE_16(STORAGE_16_BUILD_NUMBER,           		"build-number", 	VERSION_BUILD,              			VERSION_BUILD,              			VERSION_BUILD,                        0)  
  STORAGE_16(STORAGE_16_DESCARGA_TIMEOUT_SEGUNDOS,	"desc-tout",    	DESCARGA_TIMEOUT_DEFAULT_VALOR,  	DESCARGA_TIMEOUT_DEFAULT_MINIMO, 	DESCARGA_TIMEOUT_DEFAULT_MAXIMO,      0)
STORAGE_16_FIN

STORAGE_32_INICIO
  // STORAGE_32( variable,etiqueta,valor, minimo, maximo, opciones)
  STORAGE_32(STORAGE_32_OTA_CRC_ARCHIVO,        	"ota-crc",    	0,     														0, 																0xFFFFFFFF,           0)
  STORAGE_32(STORAGE_32_OTA_LARGO_ARCHIVO,      	"ota-largo",  	0,     														0, 																0xFFFFFFFF,           0)
STORAGE_32_FIN

STORAGE_BLOB_INICIO
  // STORAGE_BLOB( variable,  etiqueta,  string,  largo, opciones )
  STORAGE_BLOB(STORAGE_BLOB_WIFI_PWD,           "wifi-pwd",     WIFI_DEFAULT_PWD,       	WIFI_MAX_PWD_LEN,             1 )
  STORAGE_BLOB(STORAGE_BLOB_WIFI_SSID,          "ssid",         WIFI_DEFAULT_SSID,      	WIFI_MAX_SSID_LEN,            1 )
  STORAGE_BLOB(STORAGE_BLOB_OTA_DOMINIO,        "ota-dom",      DESCARGA_DEFAULT_DOMINIO, DESCARGA_MAX_DOMINIO_LEN,     0 )
STORAGE_BLOB_FIN

#endif

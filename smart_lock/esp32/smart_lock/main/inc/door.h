//ESP Peripherals
#include "driver/gpio.h"
// ESP Core
#include "esp_system.h"
#include "esp_log.h"
// ESP FreeRTOS
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
// Local
#include "lights.h"
#include "sounds.h"
// MQTT
#include "mqtt_client.h"

//static const char *TAG_door = "Door";
#define ON true
#define OFF false

#define DOOR_RELAY 27
#define DOOR_TIME_OPEN 1500

void open_door_nomqttlog();
void open_door(esp_mqtt_client_handle_t client);
void access_denied();
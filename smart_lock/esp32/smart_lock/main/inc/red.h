#ifndef _RED_H_
#define _RED_H_
#include "lwip/dns.h"
//#define RED_DEBUG

#define RED_TAMANO_DIRECCION_MAC      6
#define RED_REINTENTOS_CONEXION_WIFI 5
#define RED_WIFI_TIMEOUT_CONEXION_MS  20000 
#define RED_WIFI_TIMEOUT_PRUEBA_CONECTIVIDAD_MS  5000
#define RED_WIFI_REINTENTOS_PRUEBA_CONECTIVIDAD 5


#define WIFI_DEFAULT_SSID ""
#define WIFI_DEFAULT_PWD ""

#define WIFI_SSID "puerta-labi"
#define WIFI_PASSWORD "puerta-labi"

#define WIFI_MAX_SSID_LEN (uint16_t) 20
#define WIFI_MAX_PWD_LEN  (uint16_t) 30

#define RED_PERIODO_KEEPALIVE_MINUTOS 2

typedef enum
{
  RED_WIFI_MODO_AP,
  RED_WIFI_MODO_ESTACION
} red_wifi_modo_e;

typedef enum
{
  RED_DISPOSITIVO_ESTADO_RESET,
  RED_DISPOSITIVO_ESTADO_TIENE_IP,
  RED_DISPOSITIVO_ESTADO_CONECTADO,
  RED_DISPOSITIVO_ESTADO_DESCONECTADO,
  RED_DISPOSITIVO_ESTADO_DESHABILITADO,
} red_dispositivo_estado_e;

typedef enum
{
  RED_ESTADO_DESCONECTADO,
  RED_ESTADO_ESPERA_CONEXION_WIFI,
  RED_ESTADO_CONECTADO
}  red_estado_e;


esp_err_t red_inicializa(bool SSID_usaMAC);
esp_err_t red_wifiConecta();
esp_err_t red_wifiDesconecta();
bool red_wifiTieneIP();

esp_err_t wifi_getSSID(uint8_t* ssid);
bool wifi_setSSID(uint8_t* ssid);
esp_err_t wifi_getPassword(uint8_t* pwd);
bool wifi_setPassword(uint8_t* pwd);
esp_err_t wifi_getModo(uint8_t* modo);
esp_err_t wifi_setModo(uint8_t modo);

bool red_estaConectado();

void red_conecta(void);
void red_desconecta(void);
uint8_t* red_getDireccionMAC();

ssize_t red_send(int sock, uint8_t *buff, size_t largo);
ssize_t red_recv(int sock, uint8_t *buff, size_t largo);
bool red_socketBindConnect(int *sock, uint16_t port, uint32_t ip);
bool red_close(int sock);
bool red_consultaDNS(uint8_t* dominio, ip_addr_t *ip_Addr, bool *flagEncontrado, dns_found_callback cb);
void red_habilitaDebug(bool estado);
bool red_hayIP(void);
bool red_inicializada();


#endif

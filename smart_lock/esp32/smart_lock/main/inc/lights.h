#define LED_GREEN 12
#define LED_RED 14

//ESP Peripherals
#include "driver/gpio.h"
// ESP Core
#include "esp_system.h"
#include "esp_log.h"

#include <stdbool.h>

void light_set(bool state, int LED);
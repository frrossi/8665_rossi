#include "mqtt_client.h"
#include "sounds.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "rc522.h"
#include "card.h"
// ESP FreeRTOS
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"


// const esp_mqtt_client_config_t mqtt_cfg;
// //const esp_mqtt_client_handle_t client;

#ifndef TAG_SET 
static const char *TAG = "MQTT";
#define TAG_SET
#endif
static void log_error_if_nonzero(const char * message, int error_code);
static void topic_response(char * topic, char * data, esp_mqtt_client_handle_t client, size_t topic_len, size_t data_len, void *handler_args);
static esp_err_t mqtt_event_handler_cb(esp_mqtt_event_handle_t event, void *handler_args);
void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data);
char * get_email();

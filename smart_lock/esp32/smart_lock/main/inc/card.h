#ifndef CARD
#define CARD
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//ESP Peripherals
#include "driver/gpio.h"
// ESP Core
#include "esp_system.h"
#include "esp_log.h"
// ESP FreeRTOS
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "storage.h"
#include "mqtt_client.h"



typedef struct{
    esp_mqtt_client_handle_t mqtt_client;
    //bool* register_flag;
    void * register_queue;
} callback_args_t;

void tag_handler(uint8_t* sn, void * callback_args);

void save_card(uint8_t* sn, esp_mqtt_client_handle_t client);

void get_card(storage_32_enum, uint8_t* sn);

void resolve_access(uint8_t* sn, esp_mqtt_client_handle_t client);
void set_register_flag_from_card(bool value);
void register_user(uint8_t*sn,char* email,esp_mqtt_client_handle_t client);
#endif
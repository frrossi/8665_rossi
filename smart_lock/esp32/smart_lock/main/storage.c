/**
 * @file storage.c
 * @author Gerardo Stola
 * @date 14/6/2019
 * @brief Biblioteca NVS
 *
 */

#include <stdbool.h>
#include <esp_log.h>
#include "storage.h"
#include "inc/storage_enums_labi.h"

typedef struct
{
  uint8_t etiqueta[STORAGE_TAG_MAX_SIZE];
  uint8_t valor_default;
  uint8_t valor_minimo;
  uint8_t valor_maximo;
  uint8_t opciones;
} t_storage8;

typedef struct
{
  uint8_t etiqueta[STORAGE_TAG_MAX_SIZE];
  uint16_t valor_default;
  uint16_t valor_minimo;
  uint16_t valor_maximo;
  uint8_t opciones;
} t_storage16;

typedef struct
{
  uint8_t etiqueta[STORAGE_TAG_MAX_SIZE];
  uint32_t valor_default;
  uint32_t valor_minimo;
  uint32_t valor_maximo;
  uint8_t opciones;
} t_storage32;

typedef struct
{
  uint8_t etiqueta[STORAGE_TAG_MAX_SIZE];
  uint8_t string[STORAGE_BLOB_MAX_SIZE];
  uint8_t length;
  uint8_t opciones;
} t_storage_blob;

#define STORAGE_TAG_START_COUNTER  "restart_cnt"


#define STORAGE_8_INICIO static const t_storage8 _storage_labi8[STORAGE_8_CANTIDAD] = {
#define STORAGE_8(variable, etiqueta, valor_default, valor_minimo, valor_maximo, opciones) {etiqueta, (uint8_t)valor_default,(uint8_t)valor_minimo,(uint8_t)valor_maximo, (uint8_t) opciones},
#define STORAGE_8_FIN };

#define STORAGE_16_INICIO static const t_storage16 _storage_labi16[STORAGE_16_CANTIDAD] = {
#define STORAGE_16(variable, etiqueta, valor_default, valor_minimo, valor_maximo, opciones) {etiqueta, (uint16_t)valor_default,(uint16_t)valor_minimo,(uint16_t)valor_maximo, (uint8_t) opciones},
#define STORAGE_16_FIN };

#define STORAGE_32_INICIO static const t_storage32 _storage_labi32[STORAGE_32_CANTIDAD] = {
#define STORAGE_32(variable, etiqueta, valor_default, valor_minimo, valor_maximo, opciones) {etiqueta, (uint32_t)valor_default,(uint32_t)valor_minimo,(uint32_t)valor_maximo, (uint8_t) opciones},
#define STORAGE_32_FIN };

#define STORAGE_BLOB_INICIO static const t_storage_blob _storage_labi_blob[STORAGE_BLOB_CANTIDAD] = {
#define STORAGE_BLOB(variable, etiqueta, string , largo, opciones) {etiqueta, string,(uint8_t)largo, (uint8_t) opciones},
#define STORAGE_BLOB_FIN };

#include "inc/storage_parametros.h"

/*--->> Prototipo de funciones privadas <<--------------------------------------------------*/
bool valida_8(storage_8_enum idx, uint8_t valor);
bool valida_16(storage_16_enum idx, uint16_t valor);
bool valida_32(storage_32_enum idx, uint32_t valor);
storage_resultado set_restart_cnt(void);
storage_resultado storage_erase_8();
storage_resultado storage_erase_16();
storage_resultado storage_erase_32();
storage_resultado storage_erase_blobs();

storage_resultado storage_write_8_comparado(storage_8_enum idx, uint8_t valor, bool flag_comparar);
storage_resultado storage_write_16_comparado(storage_16_enum idx, uint16_t valor, bool flag_comparar);
storage_resultado storage_write_32_comparado(storage_32_enum idx, uint32_t valor, bool flag_comparar);
storage_resultado storage_blob_write_comparado(storage_blob_enum idx, uint8_t * str, uint16_t count, bool flag_comparar);

storage_resultado storage_init(void)
{
  const esp_partition_t* nvs_partition;
  esp_err_t err_code;
  storage_resultado resultado = STORAGE_ERROR_NVS;
  
  //inicializa flash
  err_code=nvs_flash_init();
  if( ESP_ERR_NVS_NO_FREE_PAGES == err_code )
  { 
    //particion NVS truncada requiere ser formateada
    ESP_LOGE(__FUNCTION__, "Partición truncacada, formateando...");
    nvs_partition = esp_partition_find_first(ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_DATA_NVS, NULL);
    assert(nvs_partition && "la tabla de partición requiere una partición NVS");
    ESP_ERROR_CHECK( esp_partition_erase_range(nvs_partition, 0, nvs_partition->size) );
    // Reintenta abrir
    err_code = nvs_flash_init();
  }
  if(ESP_OK != err_code)
  {
    ESP_LOGI(__FUNCTION__,"err %d",err_code );
  }
  else
  {
#ifdef STORAGE_CONTADOR_REINICIOS
    res = set_restart_cnt();
    if( res>0 )
    {
      ESP_LOGI(__FUNCTION__,"Partición inicio N %d",res );
    }
    else
    {      
      storage_set_factory();
    }
#endif
    resultado = STORAGE_OK;
  }
  return resultado;  
}  


storage_resultado storage_erase_8()
{
  storage_resultado resultado = STORAGE_ERROR_NVS;
	esp_err_t err_code;
  nvs_handle flash_handle;
  bool storage_abierto = false;
   
  /* abre la zona de memoria de 8 bits */
  err_code = nvs_open(STORAGE_8_BITS, NVS_READWRITE, &flash_handle);
  if (ESP_OK != err_code) 
  {      
    ESP_LOGE(__FUNCTION__, "nvs_open() %d", err_code);
  }
  else
  {
    storage_abierto = true;
  	err_code=nvs_erase_all(flash_handle);
  	if(ESP_OK != err_code)
  	{
    	ESP_LOGE(__FUNCTION__, "nvs_erase_all(): err_code=%x)", err_code);
  	}
  	else
  	{
	    err_code=nvs_commit(flash_handle);
	    if(ESP_OK != err_code)
	    {
	      ESP_LOGE(__FUNCTION__, "nvs_commit(): %x", err_code);
	    }
	    else
	    {
	      resultado = STORAGE_OK; 
	    }      	
 		}
	}

  if (storage_abierto)
  {
    nvs_close(flash_handle);
  }
  return resultado;
}

storage_resultado storage_write_8(storage_8_enum idx, uint8_t valor)
{
  return storage_write_8_comparado (idx, valor, true);
}

storage_resultado storage_write_8_comparado (storage_8_enum idx, uint8_t valor, bool flag_comparar)
{
  storage_resultado resultado = STORAGE_ERROR_NVS;
  bool storage_abierto = false;
  bool flag_escribir = true;
  esp_err_t err_code;
  nvs_handle flash_handle;
  uint8_t lectura;
   
  /* chequea si el indice está dentro de rango */	
  if (  idx >= STORAGE_8_CANTIDAD  )
  {    
    ESP_LOGE(__FUNCTION__, "idx=%d fuera de rango", idx);   
    resultado = STORAGE_ERROR_FUERA_DE_RANGO;    
  }
  else
  {
    /* chequea si el indice está dentro de rango */	
    if (  !valida_8( idx, valor) )
    {    
      ESP_LOGE(__FUNCTION__, "valor=%d fuera de rango (idx=%d)", valor, idx);   
      resultado = STORAGE_ERROR_FUERA_DE_RANGO;    
    }
    else
    {




     /* abre la zona de memoria de 8 bits */
      err_code = nvs_open(STORAGE_8_BITS, NVS_READWRITE, &flash_handle);
      if (ESP_OK != err_code) 
      {      
        ESP_LOGE(__FUNCTION__, "nvs_open() %d %d", err_code, idx);
      }
      else
      {
        storage_abierto = true;
        if (flag_comparar)
        {
          /* lee el valor antes de escribir, si es el mismo, descarta la escritura */
          err_code=nvs_get_u8(flash_handle, (char*)_storage_labi8[idx].etiqueta, &lectura);
          if (ESP_OK == err_code)
          {     
            if (lectura == valor)
            {
              flag_escribir = false;
              /* valor leido coincidía con el que se quiere grabar */
              resultado = STORAGE_OK;      
            }
          }
        }
        if (flag_escribir)
        {
          /* escribe en flash */
          err_code=nvs_set_u8(flash_handle, (char*)_storage_labi8[idx].etiqueta, valor); 
          if(ESP_OK != err_code)
          {
            ESP_LOGE(__FUNCTION__, "idx=%d, valor=%d err_code=%d",idx, valor,err_code);
          } 
          else
          {
            /* SIEMPRE despues llamar a set se debe ejecutar commit para garantizar
            que se escriban los cambios en memoria*/
        
            err_code=nvs_commit(flash_handle);
            if(ESP_OK != err_code)
            {
              ESP_LOGE(__FUNCTION__, "nvs_commit(): (idx=%d, valor=%d err_code=%d)",idx, valor,err_code);
            }
            else
            {
              resultado = STORAGE_OK; 
            }
          }
        }		
      }
    }
  }
  if (storage_abierto)
  {
    nvs_close(flash_handle);
  }
  return resultado;
}

storage_resultado storage_read_8(storage_8_enum idx, uint8_t* valor)
{
  uint8_t lectura;
  esp_err_t err_code;
  nvs_handle flash_handle;
  storage_resultado resultado = STORAGE_ERROR_NVS;
  bool storage_abierto = false;
  
  if(idx >= STORAGE_8_CANTIDAD)
  {
    resultado = STORAGE_ERROR_FUERA_DE_RANGO;
    ESP_LOGE(__FUNCTION__, "fuera de rango");
  }
  else	
  {
    /* abre zona de memoria de 8 bits */
    err_code = nvs_open(STORAGE_8_BITS, NVS_READWRITE, &flash_handle); 
    if(ESP_OK!=err_code)
    {
      ESP_LOGE(__FUNCTION__, "nvs_open: %d", err_code);
    }
    else
    {  
      storage_abierto = true;

      /* lee la flash */
      err_code=nvs_get_u8(flash_handle, (char*)_storage_labi8[idx].etiqueta, &lectura);   
      if (ESP_OK!=err_code)
      {
        ESP_LOGE(__FUNCTION__, "nvs_get_u8: %d", err_code);
      }
      else
      {
        *valor=lectura;
        resultado = STORAGE_OK;
      }
    }
  }
  if (storage_abierto)
  {
    nvs_close(flash_handle);
  }
  return resultado;
}


storage_resultado storage_erase_16()
{
  storage_resultado resultado = STORAGE_ERROR_NVS;
	esp_err_t err_code;
  nvs_handle flash_handle;
  bool storage_abierto = false;
   
  /* abre la zona de memoria de 16 bits */
  err_code = nvs_open(STORAGE_16_BITS, NVS_READWRITE, &flash_handle);
  if (ESP_OK != err_code) 
  {      
    ESP_LOGE(__FUNCTION__, "nvs_open() %d", err_code);
  }
  else
  {
    storage_abierto = true;
  	err_code=nvs_erase_all(flash_handle);
  	if(ESP_OK != err_code)
  	{
    	ESP_LOGE(__FUNCTION__, "nvs_erase_all(): err_code=%x)", err_code);
  	}
  	else
  	{
	    err_code=nvs_commit(flash_handle);
	    if(ESP_OK != err_code)
	    {
	      ESP_LOGE(__FUNCTION__, "nvs_commit(): %x", err_code);
	    }
	    else
	    {
	      resultado = STORAGE_OK; 
	    }      	
 		}
	}

  if (storage_abierto)
  {
    nvs_close(flash_handle);
  }
  return resultado;
}

storage_resultado storage_write_16(storage_16_enum idx, uint16_t valor)
{
  return storage_write_16_comparado(idx, valor, true);
}


storage_resultado storage_write_16_comparado(storage_16_enum idx, uint16_t valor, bool flag_comparar)
{
  storage_resultado resultado = STORAGE_ERROR_NVS;
  esp_err_t err_code;
  nvs_handle flash_handle;
  uint16_t lectura;
  bool storage_abierto = false;
  bool flag_escribir = true;
  
  /* chequea si el valor está dentro de rango */	
  if( (idx >= STORAGE_16_CANTIDAD) || !valida_16(idx, valor) )
  {    
    ESP_LOGE(__FUNCTION__, "fuera de rango valor=%d idx=%d", valor, idx);   
    resultado = STORAGE_ERROR_FUERA_DE_RANGO;    
  } 
  else
  {
    /* abre la zona de memoria de 16 bits */
    err_code = nvs_open(STORAGE_16_BITS, NVS_READWRITE, &flash_handle);
    if (ESP_OK != err_code) 
    {      
      ESP_LOGE(__FUNCTION__, "err: %d idx=%d", err_code, idx);
    }
    else
    {
      storage_abierto = true;
      if (flag_comparar)
      {
        /* lee el valor antes de escribir, si es el mismo, descarta la escritura */
        err_code=nvs_get_u16(flash_handle, (char*)_storage_labi16[idx].etiqueta, &lectura);
        if (ESP_OK == err_code)
        {
          if (lectura == valor)
          {
            
            flag_escribir = false;
            /* El storage ya contenía el valor correcto */
            resultado = STORAGE_OK;
          }
        }
      }
      if (flag_escribir)
	    {
        /* escribe en flash */
        err_code = nvs_set_u16(flash_handle, (char*)_storage_labi16[idx].etiqueta, valor); 
        if(ESP_OK != err_code)
        {
        	ESP_LOGE(__FUNCTION__, "idx=%d, valor=%d err_code=%d",idx, valor,err_code);
        } 
      	else
      	{
      
	        /* SIEMPRE despues llamar a set se debe ejecutar commit para garantizar
	        que se escriban los cambios en memoria*/
	      
	        err_code=nvs_commit(flash_handle);
	        if(ESP_OK != err_code)
	        {
	          ESP_LOGE(__FUNCTION__, "idx=%d, valor=%d err_code=%d",idx, valor,err_code);
	        }
	        else
	        {
	          resultado = STORAGE_OK; 
	        }
      	}
    	}     
    }
  }
  if (storage_abierto)
  {
    nvs_close(flash_handle);
  }
  return resultado;
}

storage_resultado storage_read_16(storage_16_enum idx, uint16_t* valor)
{
  uint16_t lectura;
  esp_err_t err_code;
  nvs_handle flash_handle;
  storage_resultado resultado = STORAGE_ERROR_NVS;
  bool storage_abierto = false;
  
  if(idx >= STORAGE_16_CANTIDAD)
  {
    resultado = STORAGE_ERROR_FUERA_DE_RANGO;
    ESP_LOGE(__FUNCTION__, "fuera de rango");
  }
  else	
  {
    /* abre zona de memoria de 16 bits */
    err_code = nvs_open(STORAGE_16_BITS, NVS_READWRITE, &flash_handle); 
    if(ESP_OK!=err_code)
    {
      ESP_LOGE(__FUNCTION__, "nvs_open() %d", err_code);
    }
    else
    {  
      storage_abierto = true;

      /* lee la flash */
      err_code=nvs_get_u16(flash_handle, (char*)_storage_labi16[idx].etiqueta, &lectura);   
      if (ESP_OK!=err_code)
      {
        ESP_LOGE(__FUNCTION__, "nvs_get_u16() %d", err_code);
      }
      else
      {
        *valor=lectura;
        resultado = STORAGE_OK;
      }
    }
  }
  if (storage_abierto)
  {
    nvs_close(flash_handle);
  }
  return resultado;
}


storage_resultado storage_erase_32()
{
  storage_resultado resultado = STORAGE_ERROR_NVS;
	esp_err_t err_code;
  nvs_handle flash_handle;
  bool storage_abierto = false;
   
  /* abre la zona de memoria de 32 bits */
  err_code = nvs_open(STORAGE_32_BITS, NVS_READWRITE, &flash_handle);
  if (ESP_OK != err_code) 
  {      
    ESP_LOGE(__FUNCTION__, "nvs_open() %x", err_code);
  }
  else
  {
    storage_abierto = true;
  	err_code=nvs_erase_all(flash_handle);
  	if(ESP_OK != err_code)
  	{
    	ESP_LOGE(__FUNCTION__, "nvs_erase_all(): err_code=%x)", err_code);
  	}
  	else
  	{
	    err_code=nvs_commit(flash_handle);
	    if(ESP_OK != err_code)
	    {
	      ESP_LOGE(__FUNCTION__, "nvs_commit(): %x", err_code);
	    }
	    else
	    {
	      resultado = STORAGE_OK; 
	    }      	
 		}
	}

  if (storage_abierto)
  {
    nvs_close(flash_handle);
  }
  return resultado;
}

storage_resultado storage_write_32(storage_32_enum idx, uint32_t valor)
{
  return storage_write_32_comparado(idx, valor, true);
}


storage_resultado storage_write_32_comparado(storage_32_enum idx, uint32_t valor, bool flag_comparar)
{
  storage_resultado resultado = STORAGE_ERROR_NVS;
  esp_err_t err_code;
  nvs_handle flash_handle;
  uint32_t lectura;
  bool storage_abierto = false;
	bool flag_escribir = true;  

  /* chequea si el valor está dentro de rango */	
  if( (idx >= STORAGE_32_CANTIDAD) || !valida_32(idx, valor) )
  {    
    ESP_LOGE(__FUNCTION__, "fuera de rango valor=%d idx=%d", valor, idx);   
    resultado = STORAGE_ERROR_FUERA_DE_RANGO;    
  } 
  else
  {
    /* abre la zona de memoria de 32 bits */
    err_code = nvs_open(STORAGE_32_BITS, NVS_READWRITE, &flash_handle);
    if (ESP_OK != err_code) 
    {      
      ESP_LOGE(__FUNCTION__, "err: %d", err_code);
    }
    else
    {
      storage_abierto = true;
      if (flag_comparar)
      {
        /* lee el valor antes de escribir, si es el mismo, descarta la escritura */
        err_code=nvs_get_u32(flash_handle, (char*)_storage_labi32[idx].etiqueta, &lectura);
        if (ESP_OK == err_code)
        {
          if (lectura == valor)
          {    	
            flag_escribir = false;
            /* El storage ya contenía el valor correcto */
            resultado = STORAGE_OK;
          }
        }
      }
	    if (flag_escribir)
	    {	
        /* escribe en flash */
        err_code=nvs_set_u32(flash_handle, (char*)_storage_labi32[idx].etiqueta, valor); 
        if(ESP_OK != err_code)
        {
        	ESP_LOGE(__FUNCTION__, "idx=%d, valor=%d err_code=%d",idx, valor,err_code);
        } 
				else
				{
	        /* SIEMPRE despues llamar a set se debe ejecutar commit para garantizar
	        que se escriban los cambios en memoria*/
	      
	        err_code=nvs_commit(flash_handle);
	        if(ESP_OK != err_code)
	        {
	          ESP_LOGE(__FUNCTION__, "idx=%d, valor=%d err_code=%d",idx, valor,err_code);
	        }
	        else
	        {
	          resultado = STORAGE_OK; 
	        }
      	}
      }
    }      
  }
  if (storage_abierto)
  {
    nvs_close(flash_handle);
  }
  return resultado;
}

storage_resultado storage_read_32(storage_32_enum idx, uint32_t* valor)
{
  uint32_t lectura;
  esp_err_t err_code;
  nvs_handle flash_handle;
  storage_resultado resultado = STORAGE_ERROR_NVS;
  bool storage_abierto = false;
  
  if(idx >= STORAGE_32_CANTIDAD)
  {
    resultado = STORAGE_ERROR_FUERA_DE_RANGO;
    ESP_LOGE(__FUNCTION__, "fuera de rango");
  }
  else	
  {
    /* abre zona de memoria de 32 bits */
    err_code = nvs_open(STORAGE_32_BITS, NVS_READWRITE, &flash_handle); 
    if(ESP_OK!=err_code)
    {
      ESP_LOGE(__FUNCTION__, "nvs_open() %d", err_code);
    }
    else
    {  
      storage_abierto = true;

      /* lee la flash */
      err_code=nvs_get_u32(flash_handle, (char*)_storage_labi32[idx].etiqueta, &lectura);   
      if (ESP_OK != err_code)
      {
        ESP_LOGE(__FUNCTION__, "nvs_get_u32() %d", err_code);
      }
      else
      {
        *valor=lectura;
        resultado = STORAGE_OK;
      }
    }
  }
  if (storage_abierto)
  {
    nvs_close(flash_handle);
  }
  return resultado;
}


storage_resultado storage_erase_blobs()
{
  storage_resultado resultado = STORAGE_ERROR_NVS;
	esp_err_t err_code;
  nvs_handle flash_handle;
  bool storage_abierto = false; 
  /* abre la zona de memoria de blobs */
  err_code = nvs_open(STORAGE_BLOB_NS, NVS_READWRITE, &flash_handle);
  if (ESP_OK != err_code) 
  {      
    ESP_LOGE(__FUNCTION__, "nvs_open() %x", err_code);
  }
  else
  {
    storage_abierto = true;
  	err_code=nvs_erase_all(flash_handle);
  	if(ESP_OK != err_code)
  	{
    	ESP_LOGE(__FUNCTION__, "nvs_erase_all(): err_code=%x)", err_code);
  	}
  	else
  	{
	    err_code=nvs_commit(flash_handle);
	    if(ESP_OK != err_code)
	    {
	      ESP_LOGE(__FUNCTION__, "nvs_commit(): %x", err_code);
	    }
	    else
	    {
	      resultado = STORAGE_OK; 
	    }      	
 		}
	}

  if (storage_abierto)
  {
    nvs_close(flash_handle);
  }
  return resultado;
}

storage_resultado storage_blob_write(storage_blob_enum idx, uint8_t * str, uint16_t count)
{
  return storage_blob_write_comparado(idx, str, count, true);
}


storage_resultado storage_blob_write_comparado(storage_blob_enum idx, uint8_t * str, uint16_t count, bool flag_comparar)
{

  storage_resultado resultado = STORAGE_ERROR_NVS;
  bool storage_abierto = false;
  bool flag_escribir = true;
  esp_err_t err_code;
  nvs_handle flash_handle;
  uint8_t * p;
 
  p=str;
  uint8_t lectura[STORAGE_BLOB_MAX_SIZE];
  uint8_t lectura_len;
  
  if ((idx >= STORAGE_BLOB_CANTIDAD) || (count > STORAGE_BLOB_MAX_SIZE) )
  {
   
    ESP_LOGE(__FUNCTION__, "idx=%d, count=%d",idx, count);
    resultado = STORAGE_ERROR_FUERA_DE_RANGO;
  }
  else 
  {
    err_code=nvs_open(STORAGE_BLOB_NS, NVS_READWRITE, &flash_handle);
    if(ESP_OK != err_code)
    {
      ESP_LOGE(__FUNCTION__, "nvs_open err=%d str=%s", err_code, str);
    }
    else
    {
      storage_abierto = true;
      
      /// lee el valor antes de escribir, si es el mismo, omite la escritura
      lectura_len = count;
      
      if (flag_comparar)
      {
        err_code=nvs_get_blob(flash_handle,(char *)_storage_labi_blob[idx].etiqueta, lectura, (size_t *)&lectura_len);
        if (ESP_OK == err_code)
        {
          if (lectura_len == (uint8_t) count)
          {
            if (0 == memcmp (lectura, p, count))
            {
              flag_escribir = false;
              /* El storage ya contenía el valor correcto */
              resultado = STORAGE_OK;
            }
          }
        }
      }
      if (flag_escribir)
      {
        // el blob es distinto al existente: hay que actualizarlo
        err_code=nvs_set_blob(flash_handle,(char *)_storage_labi_blob[idx].etiqueta, p, (uint8_t) count);
        if (ESP_OK != err_code)
        {
          ESP_LOGE(__FUNCTION__, "nvs_set_blob err: %d idx=%d", err_code,idx);
        }
        else
        {
          // grabó bien
          resultado = STORAGE_OK;
        }
      }
      else
      {
        // no hace falta actualizar
        resultado = STORAGE_OK;
      }
  	}   
  }
  if (storage_abierto)
  {
    nvs_close(flash_handle);
  }
  return resultado; 
}

storage_resultado storage_blob_read(storage_blob_enum idx, uint8_t * str, uint16_t *str_largo_maximo)
{
  storage_resultado resultado = STORAGE_ERROR_NVS;
  bool storage_abierto = false;
  esp_err_t err_code;
  nvs_handle flash_handle;
  uint8_t * p;
  size_t largo;
  
  if ((idx >= STORAGE_BLOB_CANTIDAD) || (*str_largo_maximo > STORAGE_BLOB_MAX_SIZE) || (*str_largo_maximo <= 0)) 
  {   
    ESP_LOGE(__FUNCTION__, "idx=%d, *str_largo_maximo=%d",idx, *str_largo_maximo);
    resultado = STORAGE_ERROR_FUERA_DE_RANGO;
  }
  else
  {
    err_code=nvs_open(STORAGE_BLOB_NS, NVS_READWRITE, &flash_handle);
    if(ESP_OK != err_code)
    {
      ESP_LOGE(__FUNCTION__, "nvs_open() %d", err_code);
    }
    else
    {
      storage_abierto = true;
      p = str;
      //largo=(size_t)*str_largo_maximo;
      
      
      largo = 1;
      /* Lee con NULL el largo de blob */
      err_code=nvs_get_blob(flash_handle,(char *)_storage_labi_blob[idx].etiqueta, NULL, (size_t*) &largo);
#if STORAGE_DEBUG      
      ESP_LOGI(__FUNCTION__, "Leyendo %s largo %d", _storage_labi_blob[idx].etiqueta, largo);
#endif
      if (ESP_OK != err_code)
      {
        ESP_LOGE(__FUNCTION__, "nvs_get_blob err: %x %s", err_code, p);
      }
      else
      {
        
        if (largo > *str_largo_maximo)
	      {
          resultado = STORAGE_ERROR_FUERA_DE_RANGO;
        }
        else
       
        {
          //ESP_LOGI(__FUNCTION__, "Leyendo %s largo %d", _storage_labi_blob[idx].etiqueta, largo);
          err_code=nvs_get_blob(flash_handle,(char *)_storage_labi_blob[idx].etiqueta, p, (size_t*) &largo);
          if (ESP_OK != err_code)
          {
            ESP_LOGE(__FUNCTION__, "nvs_get_blob() %d", err_code);
          }
          else
          {
            /* leyó bien */
            resultado = STORAGE_OK;
            *str_largo_maximo = largo;
      	  }
      	}
      }			
    }
  }
  if (storage_abierto)
  {
    nvs_close(flash_handle);
  }
  return resultado;
}

/**
 * @brief       Validador de rango para parámetros de 8 bits
 * @param i     Posición en la estructura de datos
 * @param valor	Valor a chequear (máximo y mínimo los obtiene de los valores en memoria de programa)
 * @retval      true si la validación es correcta, false de lo contrario
 */ 
bool valida_8(storage_8_enum i, uint8_t valor)
{
  bool resultado = false;
  
  if(i < STORAGE_8_CANTIDAD)
  {
    if((valor <= _storage_labi8[i].valor_maximo) &&
      ( valor >= _storage_labi8[i].valor_minimo)) 
    {
      resultado = true;
    }  
  }
  return resultado;
}


/**
 * @brief       Validador de rango para parámetros de 16 bits
 * @param i     Posición en la estructura de datos
 * @param valor	Valor a chequear (máximo y mínimo los obtiene de los valores en memoria de programa)
 * @retval      true si la validación es correcta, false de lo contrario
 */ 
bool valida_16(storage_16_enum i, uint16_t valor)
{
  bool resultado = false;
  
  if(i < STORAGE_16_CANTIDAD)
  {
    if((valor <= _storage_labi16[i].valor_maximo) &&
      ( valor >= _storage_labi16[i].valor_minimo)) 
    {
      resultado = true;
    }  
  }
  return resultado;
}


/**
 * @brief       Validador de rango para parámetros de 32 bits
 * @param i     Posición en la estructura de datos
 * @param valor	Valor a chequear (máximo y mínimo los obtiene de los valores en memoria de programa)
 * @retval      true si la validación es correcta, false de lo contrario
 */ 
bool valida_32(storage_32_enum i, uint32_t valor)
{
  bool resultado = false;
  
  if(i < STORAGE_32_CANTIDAD)
  {
    if((valor <= _storage_labi32[i].valor_maximo) &&
      ( valor >= _storage_labi32[i].valor_minimo)) 
    {
      resultado = true;
    }  
  }
  return resultado;
}

storage_resultado set_restart_cnt(void)
{
  storage_resultado resultado = STORAGE_ERROR_NVS;
  //@@ revisar si ahace falta este contador
  /*
  
  esp_err_t err_code;
  int32_t valor=0;
  nvs_handle flash_handle; 
  
  //open storage
  err_code=nvs_open(STORAGE_SYSTEM,NVS_READWRITE,&flash_handle);
  if(ESP_OK == err_code)
  {
    err_code=nvs_get_i32(flash_handle,STORAGE_TAG_START_COUNTER,&valor);
    if(ESP_OK != err_code)
    {
      ESP_LOGE(__FUNCTION__, "idx=%d, valor=%d",idx, valor);
      valor=0;
      err_code=nvs_set_i32(flash_handle,STORAGE_TAG_START_COUNTER,valor);
      err_code=nvs_commit(flash_handle);
      return valor;
    }
    else
    {
      valor++;
      err_code=nvs_set_i32(flash_handle,STORAGE_TAG_START_COUNTER,valor);
      err_code=nvs_commit(flash_handle);
    }
  }
  else
  {
    return -1;
  }
  
  */
  return resultado;
}

storage_resultado storage_set_factory(void)
{
  uint8_t i;
  storage_resultado resultado = STORAGE_ERROR_NVS;
  
  ESP_LOGI(__FUNCTION__,"Carga valores por defecto");
 
#if 0
  resultado = storage_erase_8();
  if(STORAGE_OK != resultado)
  {
    ESP_LOGE(__FUNCTION__, "storage_erase_8()");	  
  }
  else
  {
    resultado = storage_erase_16();
    if(STORAGE_OK != resultado)
    {
     ESP_LOGE(__FUNCTION__, "storage_erase_16()");	  
    }
    else
    {
      resultado = storage_erase_32();
      if(STORAGE_OK != resultado)
      {
        ESP_LOGE(__FUNCTION__, "storage_erase_32()");	  
      }
      else
      {
        resultado = storage_erase_blobs();
        if(STORAGE_OK != resultado)
        {
          ESP_LOGE(__FUNCTION__, "storage_erase_blobs()");	  
        }
        else
        {
          for(i=0 ; i<STORAGE_8_CANTIDAD ; i++)
          {
            resultado = storage_write_8(i,_storage_labi8[i].valor_default);
            if(STORAGE_OK != resultado)
            {
              ESP_LOGE(__FUNCTION__, "Falla 8 N=%d",i);
              break;
            }
          }
          if (STORAGE_OK == resultado)
          {  
            for(i=0 ; i<STORAGE_16_CANTIDAD ; i++)
            {
              resultado = storage_write_16(i,_storage_labi16[i].valor_default);
              if(STORAGE_OK != resultado)
              {
                ESP_LOGE(__FUNCTION__,"Falla 16 N=%d",i);
                break;
              }
            }   
            if (STORAGE_OK == resultado)
            {
  
              for(i=0 ; i<STORAGE_32_CANTIDAD ; i++)
              {
                resultado = storage_write_32(i,_storage_labi32[i].valor_default);
                if(STORAGE_OK != resultado)
                { 
                  ESP_LOGE(__FUNCTION__,"Falla 32=%d",i);
                  break;
                }
              }
              if (STORAGE_OK == resultado)
              { 
                for(i=0 ; i<STORAGE_BLOB_CANTIDAD ; i++)
                {
	          
                  resultado = storage_blob_write(i, (uint8_t *)_storage_labi_blob[i].string,
                   _storage_labi_blob[i].length);
                  if(STORAGE_OK != resultado)
                  {
                    ESP_LOGE(__FUNCTION__,"Falla blob N=%d (%s)",i , _storage_labi_blob[i].string);
                    break;
                  }
                }
              }
            }
          }			
        }			
      }
    }
  }
#else  
  for(i=0 ; i< STORAGE_8_CANTIDAD ; i++)
  {
    uint8_t valor;
    //ESP_LOGI(__FUNCTION__,"opciones %d %d\n",i,_storage_labi8[i].opciones);
    resultado = storage_read_8(i, &valor);
    
    
    if( (STORAGE_OK == resultado) && (1 == _storage_labi8[i].opciones))
    { /* encontró la clave y no hay que sobreescribir el valor */
    }
    else
    { /* no encontró la clave o sí hay que sobreescribir */
      resultado = storage_write_8_comparado(i,_storage_labi8[i].valor_default, false);
      if(STORAGE_OK != resultado)
      {
        ESP_LOGE(__FUNCTION__, "8b #%d",i);
        break;
      }
    }
  }
  if (STORAGE_OK == resultado)
  {  
    for(i=0 ; i<STORAGE_16_CANTIDAD ; i++)
    {
      uint16_t valor;
      //ESP_LOGI(__FUNCTION__,"opciones %d %d\n",i,_storage_labi16[i].opciones);
      resultado = storage_read_16(i, &valor);
      if( (STORAGE_OK == resultado) && (1 == _storage_labi16[i].opciones))
      { /* encontró la clave y no hay que sobreescribir el valor */
      }
      else
      { /* no encontró la clave o sí hay que sobreescribir */    
        resultado = storage_write_16_comparado(i,_storage_labi16[i].valor_default, false);
        if(STORAGE_OK != resultado)
        {
          ESP_LOGE(__FUNCTION__,"16b #%d",i);
          break;
        }
      }
    }   
    if (STORAGE_OK == resultado)
    {
      uint32_t valor;
      //ESP_LOGI(__FUNCTION__,"opciones %d %d\n",i,_storage_labi32[i].opciones);
      resultado = storage_read_32(i, &valor);
      if( (STORAGE_OK == resultado) && (1 == _storage_labi16[i].opciones))
      { /* encontró la clave y no hay que sobreescribir el valor */
      }
      else
      { /* no encontró la clave o sí hay que sobreescribir */    
        for(i=0 ; i<STORAGE_32_CANTIDAD ; i++)
        {
          resultado = storage_write_32_comparado(i,_storage_labi32[i].valor_default,false);
          if(STORAGE_OK != resultado)
          { 
            ESP_LOGE(__FUNCTION__,"32b #%d",i);
            break;
          }
        }
      }
      if (STORAGE_OK == resultado)
      { 
        for(i=0 ; i<STORAGE_BLOB_CANTIDAD ; i++)
        {       
          uint8_t valor[STORAGE_BLOB_MAX_SIZE];
          uint16_t largo = _storage_labi_blob[i].length;
          resultado = storage_blob_read(i, valor, &largo);          
          if( (STORAGE_OK == resultado) && (1 == _storage_labi_blob[i].opciones))
          { /* encontró la clave y no hay que sobreescribir el valor */        
          }
          else
          { /* no encontró la clave o sí hay que sobreescribir */    
            resultado = storage_blob_write_comparado(i, (uint8_t *)_storage_labi_blob[i].string,
             _storage_labi_blob[i].length, false);
            if(STORAGE_OK != resultado)
            {
              ESP_LOGE(__FUNCTION__,"blob #%d",i);
              break;
            }
          }
        }
      }
    }
  }
#endif  
  return resultado;
}

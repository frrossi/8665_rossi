#include "sounds.h"

static const char *TAG_sound = "Sound";

// reps define la cantidad de repeticiones del sonido
// rings_interval_ms el intervalo entre ring y ring en milisegundos
// ring_duration_ms la duración de cada ring en milisegundos
// en el futuro, vector de on offs?
void buzzer_ring(size_t reps, size_t ring_duration_ms, size_t ring_interval_ms){
    ESP_LOGI(TAG_sound, "Ringing buzzer\n");

    for (size_t i=0; i < reps; i++){
        gpio_set_level(BUZZER_GPIO, 0);
        vTaskDelay(ring_duration_ms / portTICK_PERIOD_MS);
        gpio_set_level(BUZZER_GPIO, 1);
        vTaskDelay(ring_interval_ms / portTICK_PERIOD_MS);
    }
    return;
}
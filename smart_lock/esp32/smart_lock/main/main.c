/**
 * @file main.c
 * @author Francisco Rossi
 * @date 08/12/2019
 * @brief main file for smart lock
 *
 */

// Standard Libs
#include <stdio.h>

// ESP Core
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
// ESP FreeRTOS
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
//ESP Peripherals
#include "driver/gpio.h"
#include "esp_spi_flash.h"
#include "storage.h"
// ESP Protocols
#include "mqtt.h"
#include "mqtt_client.h"

// wifi config
#include "inc/smartconfig_main.h"
// RC522
#include "rc522.h"

#include "inc/sounds.h"
#include "lights.h"
#include "card.h"
#include "door.h"

#ifndef TAG_SET 
static const char *TAG = "SMART LOCK";
#define TAG_SET
#endif

#define RC522_FLAG

xQueueHandle register_queue = NULL;


void app_main(void)
{
    
    //xQueueSend
    //xQueueReceive

    // inicialización de GPIOs
    gpio_reset_pin(BUZZER_GPIO);
    gpio_set_direction(BUZZER_GPIO, GPIO_MODE_OUTPUT);
    gpio_set_level(BUZZER_GPIO, 1);
    gpio_reset_pin(LED_GREEN);
    gpio_set_direction(LED_GREEN, GPIO_MODE_OUTPUT);
    gpio_set_level(LED_GREEN, 0);
    gpio_reset_pin(LED_RED);
    gpio_set_direction(LED_RED, GPIO_MODE_OUTPUT);
    gpio_set_level(LED_RED, 0);
    gpio_reset_pin(DOOR_RELAY);
    gpio_set_direction(DOOR_RELAY, GPIO_MODE_OUTPUT);
    gpio_set_level(DOOR_RELAY, 0);

    

    // check storage state
    // if( STORAGE_OK != storage_init() )
    //     {
    //     ESP_LOGE(__FUNCTION__, " ERROR");
    //     while (1)
    //     {
    //         ;
    //     }
    // }
    // // 
    ESP_ERROR_CHECK( nvs_flash_init() );

    // // Initialise Wi-Fi Connection (VIA cellphone app)
    initialise_wifi();
    esp_err_t err = esp_wifi_get_ps(WIFI_PS_NONE);

    vTaskDelay(20000/ portTICK_PERIOD_MS); // delay to get some time to set up wifi

    //MQTT init connection, configuración
    const esp_mqtt_client_config_t mqtt_cfg = {
        .host = "192.168.0.121",
        //.host = "192.168.0.60",
        //.host = "192.168.1.8",
        .port = 1883,
        .transport = MQTT_TRANSPORT_OVER_TCP,
        .username = "pepe",
        .password = "jorge123",
        .task_prio = 2
    };


    const esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, (void *) register_queue);
    esp_mqtt_client_start(client);
    char* topic = "/labi/main_door/#";
    esp_mqtt_client_publish(client, "/", "Smart Lock ON", 5, 1, false);
    esp_mqtt_client_subscribe(client, topic, 1);
    //esp_mqtt_client_publish(client, "/", "Smart Lock ON", 5, 1, false);
    //esp_mqtt_client_subscribe(client, '/labi/main_door/register', 1);
    //esp_mqtt_client_publish(client, "/labi/users", "hola", 5, 0, false);

    callback_args_t callback_args = {
        .mqtt_client = client,
        .register_queue = (void*) register_queue
    };

    // rcc config
    rc522_start_args_t start_args = {
        .miso_io  = RC522_DEFAULT_MISO,
        .mosi_io  = RC522_DEFAULT_MOSI,
        .sck_io   = RC522_DEFAULT_SCK,
        .sda_io   = RC522_DEFAULT_SDA,
        .callback = &tag_handler,
        .callback_args = (void*)&callback_args
    };

    rc522_start(start_args);
}   

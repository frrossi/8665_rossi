#include "door.h"
#include "sounds.h"
#include "lights.h"
#include "mqtt.h"
//ESP Peripherals
#include "driver/gpio.h"

void open_door(esp_mqtt_client_handle_t client){
    //light_set(ON, LED_GREEN);
    buzzer_ring(3, 100, 25);
    char * welcome_phrase = "Welcome to LABI";
    esp_mqtt_client_publish(client, "/labi/response", welcome_phrase, strlen(welcome_phrase), 0, false);
    gpio_set_level(DOOR_RELAY, ON);
    vTaskDelay(DOOR_TIME_OPEN /portTICK_RATE_MS);
    gpio_set_level(DOOR_RELAY, OFF);
    //light_set(OFF, LED_GREEN);
    return;
}

void open_door_nomqttlog(){
    light_set(ON, LED_GREEN);
    buzzer_ring(3, 100, 25);
    gpio_set_level(DOOR_RELAY, ON);
    vTaskDelay(DOOR_TIME_OPEN /portTICK_RATE_MS);
    gpio_set_level(DOOR_RELAY, OFF);
    light_set(OFF, LED_GREEN);
    return;
}

void access_denied(){
    light_set(true, LED_RED);
    buzzer_ring(1, 1000, 1);
    vTaskDelay(1000 / portTICK_RATE_MS);
    light_set(false, LED_RED);
}
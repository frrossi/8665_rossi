#include "mqtt.h"
#include "door.h"


static char * email = NULL;

char * get_email(){
    return email;
}

static void log_error_if_nonzero(const char * message, int error_code)
{
    if (error_code != 0) {
        ESP_LOGE(TAG, "Last error %s: 0x%x", message, error_code);
    }
}

static void topic_response(char * topic, char * data, esp_mqtt_client_handle_t client, size_t topic_len, size_t data_len, void* handler_args){

    char * topic_ok = (char*)malloc(sizeof(char)*(topic_len));
    strncpy(topic_ok, topic, topic_len);
    char * data_ok = (char*)malloc(sizeof(char)*(data_len));
    strncpy(data_ok, data, data_len);
    topic_ok[topic_len]='\0';
    data_ok[data_len]='\0';
    
    //bool* handler_args_casted = (bool*)handler_args; // no casteasteS, ...=(bool*)handler_args?
    // xQueueHandle register_queue =  handler_args;
    // bool register_flag;

    // printf("%s\n",topic_ok);
    if(strcmp(topic_ok, "/labi/main_door/register") == 0){
        //printf("La info es: \n%s\n", data_ok);
        set_register_flag_from_card(true);
        email = data_ok;
        // register_flag = true;
        // uint32_t result = xQueueSend(register_queue, &register_flag, portMAX_DELAY);
        // printf("Resultado Queue (MQTT): %u\n", result);
        // *handler_args_casted = true;
        // if(*handler_args_casted){
        //     printf("Cambio ok\n");
        // }
    }
    else if(strcmp(topic_ok, "/labi/main_door") == 0){
        switch (atoi(data_ok)){
            case 0:
                open_door(client);
                break;
            case 1:
                access_denied();
                break;
            default:
                ESP_LOGI(TAG, "topic %s\n", data_ok);
                break;
        }
    }
    // xQueueReceive(register_queue, &register_flag, portMAX_DELAY);
    // printf("Dato recibido: %d\n", register_flag);
    // free(topic_ok);
    // free(data_ok);
}

static esp_err_t mqtt_event_handler_cb(esp_mqtt_event_handle_t event, void* handler_args){
    esp_mqtt_client_handle_t client = event->client;
    //int msg_id;
    // your_context_t *context = event->context;
    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            break;
        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            break;   
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG, "MQTT_EVENT_DATA");
            printf("TOPIC=%.*s\n", event->topic_len, event->topic);
            printf("DATA=%.*s\n", event->data_len, event->data);
            topic_response(event->topic, event->data, client, event->topic_len, event->data_len, handler_args);
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            if (event->error_handle->error_type == MQTT_ERROR_TYPE_TCP_TRANSPORT) {
                log_error_if_nonzero("reported from esp-tls", event->error_handle->esp_tls_last_esp_err);
                log_error_if_nonzero("reported from tls stack", event->error_handle->esp_tls_stack_err);
                log_error_if_nonzero("captured as transport's socket errno",  event->error_handle->esp_transport_sock_errno);
                ESP_LOGI(TAG, "Last errno string (%s)", strerror(event->error_handle->esp_transport_sock_errno));
            }
            break;
        default:
            ESP_LOGI(TAG, "Other event id:%d", event->event_id);
            break;
    }
    return 0;
}

void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    mqtt_event_handler_cb(event_data, handler_args);
    
}